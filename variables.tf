variable "project_id" {
  type        = string
  description = "project id"
}

variable "region" {
  type        = string
  description = "region"
}

variable "gke_username" {
  description = "username"
}

variable "gke_password" {
  description = "password"
}

variable "cluster_name" {
  type        = string
  description = "clustername"
}

variable "gke_num_nodes" {
  description = "gke num nodes"
}
