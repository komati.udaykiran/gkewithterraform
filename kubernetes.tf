provider "kubernetes" {
load_config_file = false

host = "https://${google_container_cluster.primary.endpoint}"

username = var.gke_username
password = var.gke_password

cluster_ca_certificate = base64decode(google_container_cluster.primary.master_auth.0.cluster_ca_certificate,)
}

# Get your cluster-info
data "google_container_cluster" "primary" {
name = "es-1-gke"
location = var.region
}

# Same parameters as kubernetes provider

provider "kubectl" {
host = "https://${data.google_container_cluster.primary.endpoint}"
#token = data.google_container_cluster.primary.access-token
cluster_ca_certificate = base64decode(data.google_container_cluster.primary.master_auth.0.cluster_ca_certificate)
load_config_file = false
}


locals {
resource_list = yamldecode(file("${path. module}/all-in-one.yaml")).items
}
resource "kubectl_manifest" "all-in-one" {
count = length(local.resource_list)
yaml_body = yamlencode(local.resource_list[count.index])
}
