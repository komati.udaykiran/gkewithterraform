/*
resource "kubernetes_storage_class" "ssd" {
  metadata {
    name = "ssd"
  }

  storage_provisioner = "kubernetes.io/gce-pd"

  parameters = {
    type = "pd-ssd"
  }
}

resource "kubernetes_persistent_volume_claim" "test" {
  metadata {
    name = "test"
  }

  spec {
    access_modes       = ["ReadWriteOnce"]
    storage_class_name = "ssd"

    resources {
      requests = {
        storage = "10Gi"
      }
    }
  }
}
*/