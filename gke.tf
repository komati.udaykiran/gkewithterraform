# VPC

resource "google_compute_network" "vpc_network" {
name = "gke-1-vpc"
project = var.project_id
auto_create_subnetworks = "true"

}

# GKE cluster
resource "google_container_cluster" "primary" {
name = "es-1-gke"
location = var.region

remove_default_node_pool = true
initial_node_count = 1


network = google_compute_network.vpc_network.name
# subnetwork = google_compute_subnetwork.subnetwork.name

master_auth {
username = var.gke_username
password = var.gke_password

client_certificate_config {
issue_client_certificate = true
}
}


}

# Separately Managed coordinator-only Node Pool
resource "google_container_node_pool" "cnp" {
name = "coordinator-only"
project = var.project_id
location = var.region
cluster = google_container_cluster.primary.name
node_count = 1

node_config {

#machine_type = "e2-custom-4-8192"
image_type = "COS"
oauth_scopes = [
"https://www.googleapis.com/auth/logging.write",
"https://www.googleapis.com/auth/monitoring",
]

labels = {
env = var.cluster_name
}

# preemptible = true
machine_type = "n1-standard-1"
tags = ["gke-node", "${var.cluster_name}-gke"]
metadata = {
disable-legacy-endpoints = "true"
}
}
}


# Separately Managed data-nodes Node Pool
resource "google_container_node_pool" "dnp" {
name = "data-nodes"
project = var.project_id
location = var.region
cluster = google_container_cluster.primary.name
node_count = 1

node_config {

#machine_type = "e2-standard-16"
image_type = "COS"
oauth_scopes = [
"https://www.googleapis.com/auth/logging.write",
"https://www.googleapis.com/auth/monitoring",
]

labels = {
env = var.cluster_name
}

# preemptible = true
machine_type = "n1-standard-1"
tags = ["gke-node", "${var.cluster_name}-gke"]
metadata = {
disable-legacy-endpoints = "true"
}
}
}