provider "google" {
  version = "~> 2.20.0"
  project = var.project_id
  region  = var.region
  
}

provider "google-beta" {
  version = "~> 3.6"

}

